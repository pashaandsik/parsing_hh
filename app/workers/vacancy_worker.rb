# frozen_string_literal: true

class VacancyWorker
  include Sidekiq::Worker

  def perform(options = {})
    Parsing::VacancyService.call(
        url:                options['url'],
        short_description:  options['short_description']
    )
  end
end
