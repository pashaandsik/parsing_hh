# frozen_string_literal: true

class JobsWorker
  include Sidekiq::Worker

  def perform(options = {})
    Parsing::JobsService.call(url: options['url'])
  end
end
