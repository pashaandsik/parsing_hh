# frozen_string_literal: true

class VacancyController < ApplicationController
  def index
    @vacancy = VacancyDecorator.decorate_collection(Vacancy.all.page(params[:page].to_i))
  end

  def show
    @vacancy = Vacancy.find(params[:id].to_i).decorate
  end
end
