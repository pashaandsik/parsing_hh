# frozen_string_literal: true

class HomeController < ApplicationController
  def index; end

  def run_parser
    @form = ::Form::SearchJobs.new(params)
    if @form.valid?
      @form.process
      redirect_to :root, notice: "запрос в обработке"
    else
      redirect_to :root, alert: @form.errors.full_messages.to_sentence
    end
  end
end
