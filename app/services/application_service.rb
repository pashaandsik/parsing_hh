# frozen_string_literal: true

class ApplicationService

  def self.call(params)
    new(params).process
  end
end
