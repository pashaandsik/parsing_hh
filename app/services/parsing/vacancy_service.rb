# frozen_string_literal: true

module Parsing
  class VacancyService < ParsingService

    def initialize(params = {})
      url = params[:url]
      # url = "https://spb.hh.ru/vacancy/18352185?query=ruby"
      @link = URI.parse(url)
      @data = open(@link)
      @html = Nokogiri::HTML(@data)
      @short_description = params[:short_description]
    end

    def process
      Vacancy.create(vacancy_params)
    end

    private

    def vacancy_params
      {
         name:              name,
         short_description: @short_description,
         description:       description,
         salary_from:       salary_from,
         salary_to:         salary_to,
         currency:          currency,
         link:              @link.to_s,
         company_name:      company_name,
         company_link:      company_link,
      }
    end

    def name
      @html.css('.b-vacancy-title').text
    end

    def description
      @html.css('.b-vacancy-desc-wrapper').to_s
    end

    def salary_from
      node = @html.css('.l-content-colum-1').css('.b-v-info-content .l-paddings')
      salary_helper(node, :from)
    end

    def salary_to
      node = @html.css('.l-content-colum-1').css('.b-v-info-content .l-paddings')
      salary_helper(node, :to)
    end

    def currency
      currency_tags = @html.css('.l-content-colum-1').css('.b-v-info-content .l-paddings meta')
      if currency_tags.present?
        currency_tags.first.attr("content").to_s
      else
        nil
      end
    end

    def company_name
      strip @html.css('.companyname a').text
    end

    def company_link
      "#{ParsingService::BASIC_URL}#{url_helper(@html.css('.companyname a'))}"
    end

    def salary_helper(node, name)
      @ary ||= node.text.split('до')
      value = @ary[name == :to ? 1 : 0]
      return nil if value.nil?
      value.delete(" ").gsub(/[А-я]|\s|\./, '')
    end
  end
end
