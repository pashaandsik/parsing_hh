# frozen_string_literal: true

module Parsing
  class JobsService < ParsingService

    def initialize(params = {})
      @url = params[:url]
    end

    def parsing_jobs(url)
      link = URI.parse(URI.encode(url))
      @data = open(link)
      @html = Nokogiri::HTML(@data)
      jobs  = @html.css('.search-result-description')
      if jobs.size > 1
        jobs.each do |node|
          VacancyWorker.perform_async(
            url: node.css('.search-result-item__head a').attr('href'),
            short_description: node.css('.search-result-item__snippet').text
          )
        end
      end
      parsing_jobs(next_page) if next_page?
    end

    def next_page?
      @html.css('.b-pager__next a').present?
    end

    def next_page
      "#{ParsingService::BASIC_URL}#{@html.css('.b-pager__next a').attr('href')&.to_s}"
    end

    def process
      parsing_jobs(@url)
    end
  end
end