# frozen_string_literal: true

class ParsingService < ApplicationService
  require 'open-uri'
  require 'nokogiri'
  require 'openssl'

  BASIC_URL = "https://spb.hh.ru".freeze

  private

  def strip(string = '')
    string.to_s.gsub(/\n|\s{2,}|\s?$/, '') unless string.nil?
  end

  def url_helper(node)
    return nil if node.blank?
    node.attr("href").to_s
  end
end
