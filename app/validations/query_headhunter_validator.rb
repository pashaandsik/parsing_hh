# frozen_string_literal: true

class QueryHeadhunterValidator < ActiveModel::EachValidator
  def validate_each(object, attribute, value)
    value = value.to_s
    case
    when value.blank?
      object.errors.add attribute, :blank
    when value  =~ %r{hh\.ru}
      object.errors.add attribute, :url_hh
    end
  end
end
