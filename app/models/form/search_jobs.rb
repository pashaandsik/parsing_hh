# frozen_string_literal: true

module Form
  class SearchJobs
    include Virtus.model
    include ActiveModel::Validations

    attr_reader :controller
    delegate :params, :t, to: :controller

    attribute :query, String

    validates :query, query_headhunter: true

    BASIC_URL = "https://spb.hh.ru/search/vacancy?text=".freeze

    def process
      value = query.gsub(/\s/, '+')
      url = "#{BASIC_URL}#{value}"
      JobsWorker.perform_async(url: url)
    end
  end
end