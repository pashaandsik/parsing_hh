class VacancyDecorator < ApplicationDecorator
  decorates :vacancy

  def name_link
    h.link_to object.name, h.vacancy_path(object.id)
  end

  def company_link
    h.link_to object.company_name, object.company_link
  end

  def link
    h.link_to "ссылка на вакансию", object.link
  end

  def salary
    currency_name =
      case object.currency
      when nil
        "з/п не указана"
      when "USD"
        "USD"
      when "RUR"
        "руб."
      else
        object.currency
      end
    from = object.salary_from.to_i > 0 ? "от #{object.salary_from}" : ""
    to = object.salary_to.to_i > 0 ? "до #{object.salary_to}" : ""
    "#{from} #{to} #{currency_name}"
  end
end
