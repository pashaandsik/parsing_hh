# frozen_string_literal: true

require 'sidekiq/web'
require 'sidekiq-scheduler/web'

Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
  [user, password] == [ENV.fetch("SIDEKIQ_USER", "admin"), ENV.fetch("SIDEKIQ_PASSWORD", "admin")]
end

Rails.application.routes.draw do
  root to: "home#index"

  resources :vacancy, only: %i[index show]

  post "/run_parser"  => "home#run_parser"
  mount Sidekiq::Web  => '/sidekiq'
end
