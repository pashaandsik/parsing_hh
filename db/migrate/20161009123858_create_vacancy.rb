# frozen_string_literal: true

class CreateVacancy < ActiveRecord::Migration[5.0]
  def change
    create_table :vacancy do |t|
      t.string :name, null: false
      t.text :short_description
      t.text :description
      t.integer :salary_from
      t.integer :salary_to
      t.string :currency
      t.string :link, null: false
      t.string :company_name, null: false
      t.string :company_link, null: false
      t.timestamps null: false
    end
  end
end
