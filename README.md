# README #
## Ruby 2.3.1 Rails 5.0.0rc1 ##
## database ##
**    postgresql 9.5
    Redis version 2.8.19**

* database preparing
**        rake db:create 
        rake db:migrate**
* gems install
 bundle


## Задача ##
Реализовать систему парсинга контента с сайта [http://spb.hh.ru](http://spb.hh.ru)
По ключевому слову ruby ([https://spb.hh.ru/search/vacancy?text=ruby](https://spb.hh.ru/search/vacancy?text=ruby)) выгрузить все найденные вакансии на всех страницах, а так же их содержимое в БД.
Весь процесс сбора и обработки информации должен проходить как фоновый процесс.

## Интерфейс ##
1. Организовать инициирование запуска процесса загрузки.
2. Реализовать минимальный интерфейс для отображения списка загруженных вакансий, стандартными средствами.
## Фоновый процесс ##
Вся фоновая работа должна быть организована средствами Sidekiq/Resque.
Выделить 2 отдельных задачи: получение и обработка страницы списка вакансий, получение и обработка страницы вакансии.

## Структура таблицы vacancy ##

Name  String название вакансии
Short_description Text краткое описание вакансии
Description Text описание вакансии
Salary_from Integer з/п от
Salary_to Integer з/п до
Currency String валюта з/п
Link String ссылка на вакансию
Company_name String название компании
Company_link String ссылка на страницу компании